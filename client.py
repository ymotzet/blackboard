import sys
import requests
import urllib.parse 

#vars
SERVERIP = "http://127.0.0.1:5000/blackboards"
# SERVERPORT = 5000
 
#Method for checking errorCode from requested JSON Object
def errorcheck(errorCode):

    #Success
    if (errorCode == 0):
        print('Success!')
        return True
    #Non-existent
    elif (errorCode == 1):
        print('ERROR - The requested blackboard does not exist!')
        exit(0)
    #Already-existent
    elif (errorCode == 2):
        print('ERROR - Name for blackboard already taken!')
        exit(0)
    #Wrong-Parameters
    elif (errorCode == 3):
        print('ERROR - Entered parameters are invalid!')
        exit(0)
    #Is-empty
    elif (errorCode == 4):
        print('ERROR - The requested blackboard is empty!')
        exit(0)
    #Invalid-Timestamp
    elif (errorCode == 5):
        print('ERROR - Timestamp invalid, pull new version of the blackboard first!')
        exit(0)
    elif (errorCode == 6):
        print('ERROR - Maximum number of Blackboards reached!')
        exit()
    #Other
    elif (errorCode == 42):
        print('ERROR - *Generic excuse*')
        exit(0)
    
        

if len(sys.argv)>1:
    #Help function
    if (sys.argv[1]=="-h") or (sys.argv[1]=="--help"):
        print ("There are 7 functions to this application.\n -c <Name> to create a new blackboard \n -di <Name> <\"Inhalt\"> to update blackboard inf \n -cl <Name> to clear info from blackboard \n -r <Name> to read blackboard info \n -s <Name> to get blackboard status \n -l to list all existing blackboards \n -d <Name> to delete a blackboard completely")

    #Create function ----------------------------------------------
    if (sys.argv[1]=="-c") and len(sys.argv)>2:
        
        #name of blackboard = 2nd given argument
        bbname = sys.argv[2]
        
        #check for an empty blackboard name
        if(bbname == ""):
            sys.stderr.write("Empty name is not allowed!")
            exit(0)
        elif(len(bbname) > 128):
            sys.stderr.write("Blackboard name too long!")
            exit(0)
            
        #Construct the request address
        url = SERVERIP
        values = {'bbname': bbname}

        #send request
        r = requests.post(url, json=values) 

        #get JSON object
        try:
            responseData = r.json()
        except ValueError:
            print("Decoding JSON has failed")
            exit(0)
        
        #Error Handling
        errorcheck(responseData)
        print("The blackboard %s was successfully created!" % bbname)     


    elif (sys.argv[1]=="-c") and len(sys.argv)<=2:
        sys.stderr.write("Name for blackboard needed!")
    
    #Display function ----------------------------------------------
    if (sys.argv[1]=="-di") and len(sys.argv)>3:
        
        #name of blackboard = 2nd given argument
        bbname = sys.argv[2]

        #content for blackboard = 3rd given argument
        bbcontent = sys.argv[3]
        
        if(len(bbcontent) > 256 ):
            sys.stderr.write("Message is too long!")
            exit(0)

        url = SERVERIP + "/" + urllib.parse.quote(bbname, safe='')
        values = {'message': bbcontent}

        #send request
        r = requests.put(url, json=values) 
        
        #get JSON object
        try:
            responseData = r.json()
        except ValueError:
            print("Decoding JSON has failed")
            exit(0)

        #Error Handling
        errorcheck(responseData)
        print("Blackboard {0:s} with content {1:s} was successfully updated".format(bbname, bbcontent))   

        #Error Handling for Answer
    elif (sys.argv[1]=="-di") and len(sys.argv)<=2:
        sys.stderr.write("Name for blackboard needed!")
    elif (sys.argv[1]=="-di") and len(sys.argv)<=3:
        sys.stderr.write("Content for blackboard needed!")



    #Clear function ----------------------------------------------
    if (sys.argv[1]=="-cl") and len(sys.argv)>2:

        #name of blackboard = 2nd given argument
        bbname = sys.argv[2]    

        #parse the blackboard name to avoid wrong characters
        url = SERVERIP + "/" + urllib.parse.quote(bbname, safe='')
        values = {'message': ""} 

        #send request
        r = requests.put(url, json=values) 

        #get JSON object
        try:
            responseData = r.json()
        except ValueError:
            print("Decoding JSON has failed")
            exit(0)

        #Error Handling
        errorcheck(responseData)
        print("Blackboard %s was successfully cleared" % bbname)       

    elif (sys.argv[1]=="-cl") and len(sys.argv)<=2:
        sys.stderr.write("Name for blackboard needed!")



    #Read function ----------------------------------------------
    if (sys.argv[1]=="-r") and len(sys.argv)>2:
        
        #name of blackboard = 2nd given argument
        bbname = sys.argv[2]
                
        #Construct the request address
        #parse the blackboard name to avoid wrong characters
        url = SERVERIP + "/" + urllib.parse.quote(bbname, safe='')

        #send request
        r = requests.get(url) 
        
        #get JSON object
        try:
            responseData = r.json()
        except ValueError:
            print("Decoding JSON has failed")
            exit(0)
       
        #Error Handling
        errorcheck(responseData[0])       

        #print the recieved blackboard
        print("Blackboard: %s" % bbname)
        print("Content: %s" % responseData[1])
        print("Last modified: %s" % responseData[2])

    elif (sys.argv[1]=="-r") and len(sys.argv)<=2:
        sys.stderr.write("Name for blackboard needed!")



    #Status function ----------------------------------------------
    if (sys.argv[1]=="-s") and len(sys.argv)>2:
        
        #name of blackboard = 2nd given argument
        bbname = sys.argv[2]
                
        #Construct the request address
        #parse the blackboard name to avoid wrong characters
        url = SERVERIP + "/" + urllib.parse.quote(bbname, safe='') + "/status"

        #send request   
        r = requests.get(url) 
        
        #get JSON object
        try:
            responseData = r.json()
        except ValueError:
            print("Decoding JSON has failed")
            exit(0)
        
        #Error Handling
        errorcheck(responseData[0])
        if responseData[1][0] == True:
            print("Blackboard is empty")
        else:
            print("Blackboard is not empty")
        print("Last modified %s" % responseData[1][1])
    elif (sys.argv[1]=="-s") and len(sys.argv)<=2:
        sys.stderr.write("Name for blackboard needed!")



    #List function ----------------------------------------------
    if (sys.argv[1]=="-l"):
        
        #Construct the request address
        url = SERVERIP

        #send request
        r = requests.get(url) 
        
        #get JSON object
        try:
            responseData = r.json()
        except ValueError:
            print("Decoding JSON has failed")
            exit(0)
        
        #Error Handling
        errorcheck(responseData[0])
        print("List of Blackboards:")
        for i in range(len(responseData[1])):
            print(responseData[1][i])


    #Delete function (only deletes 1 blackboard at a time) ----------------------------------------------
    if (sys.argv[1]=="-d") and len(sys.argv)>2:
        
        #name of blackboard = 2nd given argument
        bbname = sys.argv[2]

        #Construct the request address
        #parse the blackboard name to avoid wrong characters
        url = SERVERIP + "/" + urllib.parse.quote(bbname, safe='')


        #send request
        r = requests.delete(url) 
        
        #get JSON object
        try:
            responseData = r.json()
        except ValueError:
            print("Decoding JSON has failed")
            exit(0)

        #Error Handling
        errorcheck(responseData)

    elif (sys.argv[1]=="-d") and len(sys.argv)<=2:
        sys.stderr.write("Name for blackboard needed!")

#Discription on how to use the function on CLI
else:
    sys.stderr.write("Usage: {0} <Funktion> <Parameter>\n".format(sys.argv[0]))
