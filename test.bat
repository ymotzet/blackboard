@echo off

rem Let's test the distributed blackboard application
echo Starting all tests for client.py within the distributed blackboard application...
timeout /t 5
echo.
echo.

rem First, test each command one time:
rem  Create, Display, Read, Status, List, Clear, Delete
echo Starting first test sequence, where each parameter is used at least once:
echo.
echo.
client.py -c Blackboard1
client.py -di Blackboard1 "Hello World."
client.py -r Blackboard1
client.py -di Blackboard1 No quotations used this time.   rem this will only put 'No' into the boards content
client.py -r Blackboard1    @ Read again, since new content should be available now
client.py -s Blackboard1
client.py -l
client.py -cl Blackboard1
client.py -r Blackboard1    @ Read again, since content has been updated to empty
client.py -d Blackboard1
client.py -l     @ Look if the blackboard has been deleted there should be no blackboard
echo.
echo.
echo.
echo.
echo.
timeout /t 10
rem fully verified


rem Second, test the application's numerical limits:
rem  max. number of blackboards,
rem  min./ max. blackboard name length
rem  max. blackboard content length
echo Starting second test sequence, where the numerical limits of the application is tested
echo.
echo.
for /l %%x in (1, 1, 201) do (
	client.py -c Blackboard%%x     @expect overflow warning from the server on the last blackboard creation
)
client.py -l
timeout /t 5
rem fully verified

rem now, delete some blackboards to make space
for /l %%y in (1, 1, 10) do (
	client.py -d Blackboard%%y
)
client.py -l
rem fully verified

timeout /t 5
rem try and create a blackboard with empty name
client.py -c ""
echo.
client.py -c
echo.
client.py -l


timeout /t 5
rem create blackboard using a name that is too long
rem   1.: 120 a's
rem   2.: 128 a's
rem   3.: 129 a's
client.py -c aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
client.py -c aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
client.py -c aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa


timeout /t 5
rem fill an existing blackboard with too much content
rem   1.: 256 Unicode elements
rem   2.: 256 Unicode elements, no quotes
rem   3.: 257 Unicode elements
client.py -di Blackboard11 "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
client.py -di Blackboard11 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
client.py -di Blackboard11 "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
