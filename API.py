#!flask/bin/python
from flask import Flask, jsonify, request
from enum import Enum
import datetime
import urllib.parse


app = Flask(__name__)

#enumeration for errorcodes to be understandable to humans within the code
class ERRORCODE(Enum):
    SUCCESS = 0
    NONEXISTENT = 1
    ALREADYEXISTENT = 2
    WRONGPARAM = 3
    ISEMPTY = 4
    INVALIDTIMESTAMP = 5
    BLACKBOARDLIMITREACHED = 6
    OTHER = 42

# exceptions for blackboard class methods 
class NoBlackboard(Exception):
    pass
class BlackboardAlreadyExists(Exception):
    pass
class BlackboardOverflow(Exception):
    pass


# maximum number of blackboards supported
MAX_NUMBER_BLACKBORD = 200


# global list in which all blackboard objects are stored at runtime
all_blackboards = []


#******************************************************************
# class for the blackboards
# attributes:
#   name:       name of the blackboard
#   content:    what is written on the board
#   timestamp:  last edited at this time
#   is_empty:   boolean (true = no content)
# methods:
#   init:           used to create a blackboard with empty content
#   set_content:    change content of existing blackboard
#   get_content:    read the content of a blackboard
#   get_status:     read the status of a blackboard (is_empty)
#   get_name:       read name of the blackboard

class Blackboard:
    name = None
    content = None
    timestamp = None
    is_empty = None
    
    def __init__(self, name):
        self.name = name
        self.content = ""
        self.is_empty = True
        #utc_time = datetime.datetime.now().replace(microsecond = 0)
        # change format to 1. January 2019 10:10:10
        #time_formatted = datetime.datetime.strftime(utc_time,'%d %b %Y %H:%M:%S')
        #self.timestamp = time_formatted
        self.timestamp = setTimestamp();
        

    def set_content(self, new_content):
        if new_content == "":
            self.is_empty = True
        else:
            self.is_empty = False
        utc_time = datetime.datetime.now().replace(microsecond = 0)
        # change format to 1. January 2019 10:10:10
        time_formatted = datetime.datetime.strftime(utc_time,'%d %b %Y %H:%M:%S')
        self.timestamp = time_formatted
        self.content = new_content
        
    def get_content(self):
        return self.content

    def get_status(self):
        return self.is_empty, self.timestamp

    def get_name(self):
        return self.name


#******************************************************************
# server methods
# appends new object to all_blackboards list
def create_blackboard(name):

    # check if blackboard storage is full
    if len(all_blackboards) >= MAX_NUMBER_BLACKBORD:
        raise BlackboardOverflow('error: could not create blackboard (max number of blackboards reached)')
    # check if blackboard name already exists
    for board in all_blackboards:
        if board.get_name() == name:
            raise BlackboardAlreadyExists('error: could not create blackboard (blackboard already exists)')
    # add new blackboard to the list
    all_blackboards.append(Blackboard(name))

# looks for blackboard with provided name and returns reference
def get_blackboard_by_name(name):
    for board in all_blackboards:
        if board.get_name() == name:
            # return reference to the blackboard
            return board
    raise NoBlackboard('error: blackboard not found')

# deletes blackboard in all_blackboard
def delete_blackboard_by_name(name):
    # search for the blackboard
    for i in range(len(all_blackboards)):
        if all_blackboards[i].get_name() == name:
            # delete blackboard entry
            del all_blackboards[i]
            print ("Deleted blackboard")
            return
    raise NoBlackboard('error: blackboard not found')

# method to get the formatted timestamp
def setTimestamp():
    utc_time = datetime.datetime.now().replace(microsecond = 0)
    # change format to 1. January 2019 10:10:10
    utc_formatted = datetime.datetime.strftime(utc_time,'%d %b %Y %H:%M:%S')
    return utc_formatted

#******************************************************************
# API functions
#
# create a blackboard
@app.route('/blackboards', methods=['POST'])
def API_create_blackboard():

    # read name from JSON payload
    nameJson = request.get_json()
    name = nameJson['bbname']
    # check if name is longer than 128 characters
    if len(name) > 128:
        return jsonify(ERRORCODE['WRONGPARAM'].value)
    
    try:
        create_blackboard(name)
#        get_blackboard_by_name(name).timestamp = setTimestamp() #included in the init method
    except BlackboardAlreadyExists as e:
        print(e)
        return jsonify(ERRORCODE['ALREADYEXISTENT'].value)
    except BlackboardOverflow as e:
        print(e)
        return jsonify(ERRORCODE['BLACKBOARDLIMITREACHED'].value)
    
    return jsonify(ERRORCODE['SUCCESS'].value)


# get content of a blackboard
@app.route('/blackboards/<string:name>', methods=['GET'])
def API_get_blackboard(name):

    # reverse parsing of the name (URL compliant)
    nameDecoded = urllib.parse.unquote(name, encoding='utf-8', errors='replace')

    # read blackboard content
    try:
        contentBuffer = get_blackboard_by_name(nameDecoded).get_content()
        print(contentBuffer)
        if len(contentBuffer) == 0:
            return jsonify(ERRORCODE['ISEMPTY'].value, "")
    except NoBlackboard as e:
        print(e)
        return jsonify(ERRORCODE['NONEXISTENT'].value, "")

    # read timestamp
    timestampBuffer = get_blackboard_by_name(nameDecoded).timestamp

    return jsonify(ERRORCODE['SUCCESS'].value, contentBuffer, timestampBuffer)

# update or clear content of a blackboard
@app.route('/blackboards/<string:name>', methods=['PUT'])
def API_update_blackboard(name):

    # reverse parsing of the name (URL compliant)
    nameDecoded = urllib.parse.unquote(name, encoding='utf-8', errors='replace')
    
    # check for maximum message length
    if len(request.get_json()['message']) > 256:
        print("WARNING: content is too long.\n")
        return jsonify(ERRORCODE['WRONGPARAM'].value)

    # set blackboard content
    try:
        get_blackboard_by_name(nameDecoded).set_content(request.get_json()['message'])
 
    except NoBlackboard as e:
        print(e)
        return jsonify(ERRORCODE['NONEXISTENT'].value)

    return jsonify(ERRORCODE['SUCCESS'].value)

# fetch status of a blackboard
@app.route('/blackboards/<string:name>/status', methods=['GET'])
def API_get_blackboard_status(name):

    # reverse parsing of the name (URL compliant)
    nameDecoded = urllib.parse.unquote(name, encoding='utf-8', errors='replace')

    # read status of blackboard
    try:
        statusBuffer = get_blackboard_by_name(nameDecoded).get_status()
    except NoBlackboard as e:
        print(e)
        return jsonify(ERRORCODE['NONEXISTENT'].value, "")
    
    return jsonify(ERRORCODE['SUCCESS'].value, statusBuffer)

# list of blackboard names
@app.route('/blackboards', methods=['GET'])
def API_get_blackboard_list():

    # list for the blackboards' names
    namelist = []

    # check if no blackboards exist and add an empty string
    if len(all_blackboards) == 0:
        namelist.append("")
    # iterate over blackboards and write names to the list
    for element in all_blackboards:
        print(element.get_name())
        namelist.append(element.get_name())

    return jsonify(ERRORCODE['SUCCESS'].value, namelist)


# delete a blackboard
@app.route('/blackboards/<string:name>', methods=['DELETE'])
def API_delete_blackboard(name):

    # reverse parsing of the name (URL compliant)
    nameDecoded = urllib.parse.unquote(name, encoding='utf-8', errors='replace')

    # delete blackboard entry
    try:
        delete_blackboard_by_name(nameDecoded)
    except NoBlackboard as e:
        print(e)
        return jsonify(ERRORCODE['NONEXISTENT'].value)
    
    return jsonify(ERRORCODE['SUCCESS'].value)

#******************************************************************

if __name__ == '__main__':
    app.run()